import 'package:flutter/material.dart';
import 'package:instragram_clone/pages/account.dart';
import 'package:instragram_clone/pages/home.dart';
import 'package:instragram_clone/pages/reels.dart';
// import 'package:instragram_clone/pages/shop.dart';
import 'package:ionicons/ionicons.dart';

class HomePage extends StatefulWidget {
  const HomePage({Key? key}) : super(key: key);

  @override
  _HomePageState createState() => _HomePageState();
}

class _HomePageState extends State<HomePage> {
  int _selectedIndex = 0;

  void _onItemTapped(int index) {
    setState(() {
      _selectedIndex = index;
    });
  }

  Widget pageCaller(int index) {
    switch (index) {
      case 0:
        return UserHome();
      case 1:
        return UserReels();
      case 2:
        return UserAccount();
      default:
        return UserHome();
    }
  }

  @override
  Widget build(BuildContext context) {
    return Scaffold(
      // resizeToAvoidBottomInset: false,
      body: Center(
        child: pageCaller(_selectedIndex),
      ),
      bottomNavigationBar: BottomNavigationBar(
        showSelectedLabels: false,
        showUnselectedLabels: false,
        currentIndex: _selectedIndex,
        onTap: _onItemTapped,
        type: BottomNavigationBarType.fixed,
        items: [
          BottomNavigationBarItem(
              icon: Icon(
                Ionicons.home_outline,
                size: 30,
              ),
              label: 'Home'),
          // BottomNavigationBarItem(
          //     icon: Icon(
          //       Ionicons.search_outline,
          //       size: 30,
          //     ),
          //     label: 'Search'),
          BottomNavigationBarItem(
              icon: Icon(
                Ionicons.add_circle_outline,
                size: 35,
              ),
              label: 'Add'),
          // BottomNavigationBarItem(
          //     icon: Icon(
          //       Ionicons.heart_outline,
          //       size: 30,
          //     ),
          //     label: 'Favorites'),
          BottomNavigationBarItem(
              icon: Icon(
                Ionicons.person_circle_outline,
                size: 30,
              ),
              label: 'Profile'),
        ],
      ),
    );
  }
}
