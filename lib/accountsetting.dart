import 'package:cloud_firestore/cloud_firestore.dart';
import 'package:flutter/material.dart';

class AccountSetting extends StatefulWidget {
  const AccountSetting({Key? key}) : super(key: key);

  @override
  State<AccountSetting> createState() => _AccountSettingState();
}

class _AccountSettingState extends State<AccountSetting> {
  String description = '';

  void setDescription(value) {
    description = value;
  }

  @override
  Widget build(BuildContext context) {
    return Scaffold(
        appBar: AppBar(
          automaticallyImplyLeading: false,
          leadingWidth: 80,
          leading: TextButton(
            onPressed: () => Navigator.of(context).maybePop(),
            child: Text(
              'Cancel',
              style: TextStyle(fontSize: 17, fontWeight: FontWeight.w400),
            ),
          ),
          centerTitle: true,
          foregroundColor: Colors.black,
          title: const Text('Edit Profile',
              style: TextStyle(fontWeight: FontWeight.w900)),
          backgroundColor: const Color(0xfff2f2f2),
          shadowColor: Colors.transparent,
          // actions: [
          //   TextButton(
          //       onPressed: () {
          //         Navigator.of(context).maybePop();
          //       },
          //       child: Text(
          //         'Done',
          //         style: TextStyle(
          //             fontSize: 17,
          //             color: Colors.blue.shade600,
          //             fontWeight: FontWeight.w900),
          //       ))
          // ],
        ),
        body: SingleChildScrollView(
          child: Column(
            children: [
              StreamBuilder(
                stream:
                    FirebaseFirestore.instance.collection('users').snapshots(),
                builder: (BuildContext context,
                    AsyncSnapshot<QuerySnapshot> snapshot) {
                  if (!snapshot.hasData) {
                    return Center(
                      child: Text('Loading...'),
                    );
                  }

                  return ListView(
                      shrinkWrap: true,
                      children: snapshot.data!.docs.map((document) {
                        return Column(
                          children: [
                            ProfileImage(document: document),
                            Divider(),
                            UsernameTextField(document: document),
                            Divider(),
                            BioTextField(
                                document: document,
                                setDescription: setDescription),
                            Divider(),
                            Padding(
                              padding: const EdgeInsets.all(16.0),
                              child: Container(
                                height: 40,
                                width: 200,
                                decoration: BoxDecoration(
                                    shape: BoxShape.rectangle,
                                    border: Border.all(
                                        width: 1.0,
                                        color: Colors.grey.shade400),
                                    borderRadius: BorderRadius.circular(5)),
                                child: TextButton(
                                  onPressed: () {
                                    setState(() {
                                      DocumentReference users =
                                          FirebaseFirestore.instance
                                              .collection("users")
                                              .doc(document.id);
                                      users.update({
                                        'description': description,
                                      });
                                      Navigator.of(context).maybePop();
                                    });
                                  },
                                  child: Text(
                                    'Change',
                                    style: TextStyle(
                                        fontWeight: FontWeight.bold,
                                        color: Colors.black,
                                        fontSize: 16),
                                  ),
                                ),
                              ),
                            )
                          ],
                        );
                      }).toList());
                },
              ),
              // ProfileImage(profile: profile),
              // Divider(),
              // UsernameTextField(),
              // Divider(),
              // BioTextField(),
              // Divider(
              //   height: 10,
              //   color: Colors.grey.shade400,
              // ),
              // ChangeProfileImage(),
              // NameTextField(),
              // WebsiteTextField(),
              // Professional(),
              // PrivateInformation(),
              // EmailTextField(),
              // PhoneTextField(),
              // GenderTextField(),
            ],
          ),
        ));
  }
}

class GenderTextField extends StatelessWidget {
  const GenderTextField({
    Key? key,
  }) : super(key: key);

  @override
  Widget build(BuildContext context) {
    return Padding(
      padding: const EdgeInsets.only(left: 16.0, right: 8),
      child: Row(
        children: [
          Container(
            width: 90,
            height: 20,
            child: Text(
              'Gender',
              style: TextStyle(fontSize: 17),
            ),
          ),
          Flexible(
            child: Padding(
              padding: const EdgeInsets.only(left: 30.0, right: 8),
              child: TextFormField(
                initialValue: 'Male',
              ),
            ),
          ),
        ],
      ),
    );
  }
}

class PhoneTextField extends StatelessWidget {
  const PhoneTextField({
    Key? key,
  }) : super(key: key);

  @override
  Widget build(BuildContext context) {
    return Padding(
      padding: const EdgeInsets.only(left: 16.0, right: 8),
      child: Row(
        children: [
          Container(
            width: 90,
            height: 20,
            child: Text(
              'Phone',
              style: TextStyle(fontSize: 17),
            ),
          ),
          Flexible(
            child: Padding(
              padding: const EdgeInsets.only(left: 30.0, right: 8),
              child: TextFormField(
                initialValue: '+855 123 456 789',
              ),
            ),
          ),
        ],
      ),
    );
  }
}

class EmailTextField extends StatelessWidget {
  const EmailTextField({
    Key? key,
  }) : super(key: key);

  @override
  Widget build(BuildContext context) {
    return Padding(
      padding: const EdgeInsets.only(left: 16.0, right: 8),
      child: Row(
        children: [
          Container(
            width: 90,
            height: 20,
            child: Text(
              'Email',
              style: TextStyle(fontSize: 17),
            ),
          ),
          Flexible(
            child: Padding(
              padding: const EdgeInsets.only(left: 30.0, right: 8),
              child: TextFormField(
                initialValue: 'person1@gmail.com',
              ),
            ),
          ),
        ],
      ),
    );
  }
}

class PrivateInformation extends StatelessWidget {
  const PrivateInformation({
    Key? key,
  }) : super(key: key);

  @override
  Widget build(BuildContext context) {
    return Padding(
      padding: const EdgeInsets.all(16.0),
      child: Container(
        alignment: Alignment.centerLeft,
        child: Text(
          'Private Information',
          style: TextStyle(fontSize: 16, fontWeight: FontWeight.w900),
        ),
      ),
    );
  }
}

class Professional extends StatelessWidget {
  const Professional({
    Key? key,
  }) : super(key: key);

  @override
  Widget build(BuildContext context) {
    return Padding(
      padding: const EdgeInsets.only(left: 8.0),
      child: Container(
        alignment: Alignment.centerLeft,
        child: TextButton(
            onPressed: () {},
            child: Text(
              'Switch to Professional Account',
              style: TextStyle(
                  color: Colors.blue.shade500,
                  fontWeight: FontWeight.w400,
                  fontSize: 15),
            )),
      ),
    );
  }
}

// ignore: must_be_immutable
class BioTextField extends StatefulWidget {
  BioTextField({Key? key, required this.document, required this.setDescription})
      : super(key: key);

  final QueryDocumentSnapshot<Object?> document;
  Function setDescription;

  @override
  State<BioTextField> createState() => _BioTextFieldState();
}

class _BioTextFieldState extends State<BioTextField> {
  @override
  Widget build(BuildContext context) {
    return Padding(
      padding: const EdgeInsets.only(left: 16.0, right: 8),
      child: Row(
        children: [
          Container(
            width: 90,
            height: 20,
            child: Text(
              'Bio',
              style: TextStyle(fontSize: 17),
            ),
          ),
          Flexible(
            child: Padding(
              padding: const EdgeInsets.only(left: 30.0, right: 8),
              child: Flexible(
                child: TextFormField(
                  keyboardType: TextInputType.multiline,
                  maxLines: null,
                  initialValue: widget.document['description'],
                  onChanged: (value) {
                    widget.setDescription(value);
                  },
                ),
              ),
            ),
          ),
        ],
      ),
    );
  }
}

class WebsiteTextField extends StatelessWidget {
  const WebsiteTextField({
    Key? key,
  }) : super(key: key);

  @override
  Widget build(BuildContext context) {
    return Padding(
      padding: const EdgeInsets.only(left: 16.0, right: 8),
      child: Row(
        children: [
          Container(
            width: 90,
            height: 20,
            child: Text(
              'Website',
              style: TextStyle(fontSize: 17),
            ),
          ),
          Flexible(
            child: Padding(
              padding: const EdgeInsets.only(left: 30.0, right: 8),
              child: TextFormField(
                decoration: InputDecoration(hintText: 'Website'),
              ),
            ),
          ),
        ],
      ),
    );
  }
}

class UsernameTextField extends StatelessWidget {
  UsernameTextField({Key? key, required this.document}) : super(key: key);

  final QueryDocumentSnapshot<Object?> document;
  @override
  Widget build(BuildContext context) {
    return Padding(
      padding: const EdgeInsets.only(left: 16.0, right: 8),
      child: Row(
        children: [
          Container(
            width: 90,
            height: 20,
            child: Text(
              'Username',
              style: TextStyle(fontSize: 17),
            ),
          ),
          Flexible(
            child: Padding(
              padding: const EdgeInsets.only(left: 30.0, right: 8),
              child: TextFormField(
                readOnly: true,
                initialValue: document['name'],
                onChanged: (value) {},
              ),
            ),
          ),
        ],
      ),
    );
  }
}

class NameTextField extends StatelessWidget {
  const NameTextField({
    Key? key,
  }) : super(key: key);

  @override
  Widget build(BuildContext context) {
    return Padding(
      padding: const EdgeInsets.only(left: 16.0, right: 8),
      child: Row(
        children: [
          Container(
            width: 90,
            height: 20,
            child: Text(
              'Name',
              style: TextStyle(fontSize: 17),
            ),
          ),
          Flexible(
            child: Padding(
              padding: const EdgeInsets.only(left: 30.0, right: 8),
              child: TextFormField(
                initialValue: 'Person 01',
              ),
            ),
          ),
        ],
      ),
    );
  }
}

class ChangeProfileImage extends StatelessWidget {
  const ChangeProfileImage({
    Key? key,
  }) : super(key: key);

  @override
  Widget build(BuildContext context) {
    return Center(
      child: TextButton(
        onPressed: () {},
        child: Text(
          'Change Profile Photo',
          style: TextStyle(
              fontSize: 14,
              color: Colors.blue.shade700,
              fontWeight: FontWeight.bold),
        ),
      ),
    );
  }
}

class ProfileImage extends StatelessWidget {
  ProfileImage({
    Key? key,
    required this.document,
  }) : super(key: key);

  final QueryDocumentSnapshot<Object?> document;

  @override
  Widget build(BuildContext context) {
    return Padding(
      padding: const EdgeInsets.only(top: 20.0),
      child: Center(
        child: Container(
          width: 95,
          height: 95,
          decoration: BoxDecoration(
              shape: BoxShape.circle,
              image: DecorationImage(
                  image: NetworkImage(document['profileImage']),
                  fit: BoxFit.cover)),
        ),
      ),
    );
  }
}
