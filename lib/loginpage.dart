// ignore_for_file: prefer_const_constructors

import 'package:flutter/material.dart';

import 'homepage.dart';

// ignore: use_key_in_widget_constructors
class LoginPage extends StatefulWidget {
  @override
  _LoginPageState createState() => _LoginPageState();
}

class _LoginPageState extends State<LoginPage> {
  @override
  Widget build(BuildContext context) {
    return Scaffold(
      resizeToAvoidBottomInset: false,
      body: SafeArea(
        child: Padding(
          padding: const EdgeInsets.only(top: 24.0, left: 8.0, right: 8.0),
          child: Column(
            // mainAxisAlignment: MainAxisAlignment.center,
            children: [
              Spacer(),
              InstagramLogo(),
              UsernameInputBox(),
              PasswordInputBox(),
              ForgotPassword(),
              LoginButton(),
              FBLogin(),
              OrDivider(),
              SignUp(),
              Spacer(),
            ],
          ),
        ),
      ),
    );
  }
}

class InstagramLogo extends StatelessWidget {
  const InstagramLogo({
    Key? key,
  }) : super(key: key);

  @override
  Widget build(BuildContext context) {
    return Center(
        child: Padding(
      padding: const EdgeInsets.only(bottom: 40),
      child: SizedBox(child: Image.asset('assets/images/logo.png')),
    ));
  }
}

class UsernameInputBox extends StatelessWidget {
  const UsernameInputBox({
    Key? key,
  }) : super(key: key);

  @override
  Widget build(BuildContext context) {
    return Padding(
      padding: const EdgeInsets.all(8.0),
      child: TextFormField(
        // initialValue: "CheaVitouLay",
        decoration: InputDecoration(
            hintText: 'Username',
            border: const OutlineInputBorder(gapPadding: 4),
            fillColor: Colors.grey[900]),
      ),
    );
  }
}

class PasswordInputBox extends StatelessWidget {
  const PasswordInputBox({
    Key? key,
  }) : super(key: key);

  @override
  Widget build(BuildContext context) {
    return Padding(
      padding: const EdgeInsets.all(8.0),
      child: TextFormField(
        obscureText: true,
        autocorrect: false,
        enableSuggestions: false,
        decoration: InputDecoration(
            hintText: 'Password', border: const OutlineInputBorder()),
      ),
    );
  }
}

class ForgotPassword extends StatelessWidget {
  const ForgotPassword({
    Key? key,
  }) : super(key: key);

  @override
  Widget build(BuildContext context) {
    return Padding(
      padding: const EdgeInsets.all(8.0),
      child: Row(
        mainAxisAlignment: MainAxisAlignment.end,
        children: [
          Text(
            'forgot password?',
            style:
                TextStyle(fontWeight: FontWeight.bold, color: Colors.blue[300]),
          ),
        ],
      ),
    );
  }
}

class LoginButton extends StatelessWidget {
  const LoginButton({
    Key? key,
  }) : super(key: key);

  @override
  Widget build(BuildContext context) {
    return Padding(
      padding: const EdgeInsets.all(8.0),
      child: TextButton(
        onPressed: () {
          Navigator.push(
            context,
            MaterialPageRoute(builder: (context) => HomePage()),
          );
        },
        child: const Text(
          'Log in',
          style: TextStyle(
              fontWeight: FontWeight.bold, color: Colors.white, fontSize: 20),
        ),
        style: TextButton.styleFrom(
            padding: EdgeInsets.all(14.0),
            backgroundColor: Colors.blue[200],
            minimumSize: Size.fromHeight(40)),
      ),
    );
  }
}

class FBLogin extends StatelessWidget {
  const FBLogin({
    Key? key,
  }) : super(key: key);

  @override
  Widget build(BuildContext context) {
    return Padding(
      padding: const EdgeInsets.only(top: 30.0),
      child: Center(
        child: Row(
          mainAxisAlignment: MainAxisAlignment.center,
          children: [
            SizedBox(height: 30, child: Image.asset('assets/images/fb.png')),
            Text(
              'Log in with Facebook',
              style: TextStyle(
                  fontSize: 16,
                  color: Colors.blue,
                  fontWeight: FontWeight.bold),
            ),
          ],
        ),
      ),
    );
  }
}

class OrDivider extends StatelessWidget {
  const OrDivider({
    Key? key,
  }) : super(key: key);

  @override
  Widget build(BuildContext context) {
    return Padding(
      padding: const EdgeInsets.only(top: 30.0),
      child: Row(
        children: [
          Expanded(
              child: Container(
            margin: EdgeInsets.only(right: 30, left: 8),
            child: Divider(
              height: 1,
            ),
          )),
          Text(
            'OR',
            style: TextStyle(
                fontWeight: FontWeight.bold,
                fontSize: 16,
                color: Color.fromRGBO(0, 0, 0, 0.4)),
          ),
          Expanded(
              child: Container(
            margin: EdgeInsets.only(left: 30, right: 8),
            child: Divider(
              height: 1,
            ),
          )),
        ],
      ),
    );
  }
}

class SignUp extends StatelessWidget {
  const SignUp({
    Key? key,
  }) : super(key: key);

  @override
  Widget build(BuildContext context) {
    return Padding(
      padding: const EdgeInsets.only(top: 30.0),
      child: Center(
        child: Row(
          mainAxisAlignment: MainAxisAlignment.center,
          children: [
            Text('Don\'t have an account?',
                style: TextStyle(
                    fontSize: 20, color: Color.fromRGBO(0, 0, 0, 0.4))),
            TextButton(
                onPressed: () {},
                child: Text(
                  'Sign up.',
                  style: TextStyle(
                      fontSize: 19,
                      color: Colors.blue[200],
                      fontWeight: FontWeight.w400),
                ))
          ],
        ),
      ),
    );
  }
}

class Footer extends StatelessWidget {
  const Footer({
    Key? key,
  }) : super(key: key);

  @override
  Widget build(BuildContext context) {
    return SizedBox(
      height: 90,
      child: Column(
        children: [
          Divider(),
          Padding(
            padding: const EdgeInsets.only(top: 25),
            child: Text('Instagram of Facebook'),
          )
        ],
      ),
    );
  }
}
