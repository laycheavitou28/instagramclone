import 'package:flutter/material.dart';

class BubbleStories extends StatelessWidget {
  final String text;
  final String img;
  BubbleStories({required this.text, required this.img});
  // BubbleStories({required this.text} {required this.img});
  @override
  Widget build(BuildContext context) {
    return Padding(
        padding: const EdgeInsets.all(8.0),
        child: Column(
          children: [
            Container(
              width: 65,
              height: 65,
              decoration: BoxDecoration(
                  shape: BoxShape.circle,
                  gradient: LinearGradient(colors: [
                    Colors.purple,
                    Colors.purpleAccent,
                    Colors.purple,
                    Colors.orangeAccent,
                    Colors.pink,
                    Colors.pinkAccent,
                    Colors.pink,
                    Colors.purple,
                    Colors.purpleAccent,
                  ])),
              child: Stack(
                children: [
                  Container(
                    decoration: BoxDecoration(
                      shape: BoxShape.circle,
                    ),
                    child: Center(
                      child: ClipRRect(
                        borderRadius: BorderRadius.circular(35),
                        child: Image.asset(
                          img,
                          fit: BoxFit.fill,
                          width: 60,
                          height: 60,
                        ),
                      ),
                    ),
                  )
                ],
              ),
            ),
            SizedBox(height: 10),
            Text(text)
          ],
        ));
  }
}
