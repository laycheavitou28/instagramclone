import 'package:cloud_firestore/cloud_firestore.dart';
import 'package:flutter/material.dart';
import 'package:ionicons/ionicons.dart';
import 'package:carousel_slider/carousel_slider.dart';

class UserPosts extends StatefulWidget {
  final QueryDocumentSnapshot<Object?> document;
  UserPosts({required this.document});

  @override
  State<UserPosts> createState() => _UserPostsState();
}

class _UserPostsState extends State<UserPosts> {
  @override
  Widget build(BuildContext context) {
    final post = widget.document;
    return Column(
      crossAxisAlignment: CrossAxisAlignment.start,
      children: [
        Padding(
          padding: const EdgeInsets.all(12),
          child: Row(
            mainAxisAlignment: MainAxisAlignment.spaceBetween,
            children: [
              Row(
                children: [
                  // profile photo
                  Container(
                    width: 45,
                    height: 45,
                    decoration: BoxDecoration(
                        shape: BoxShape.circle,
                        image: DecorationImage(
                            image: NetworkImage(widget.document['profile']),
                            fit: BoxFit.fill)),
                  ),
                  SizedBox(
                    width: 10,
                  ),
                  //name
                  Row(
                    children: [
                      Text(
                        widget.document['name'],
                        // ignore: prefer_const_constructors
                        style: TextStyle(fontWeight: FontWeight.bold),
                      ),
                      Padding(
                        padding: const EdgeInsets.only(left: 4.0),
                        child: Image.asset(
                          'assets/images/check.png',
                        ),
                      )
                    ],
                  ),
                ],
              ),
              IconButton(
                splashRadius: 16,
                onPressed: () {},
                icon: Icon(Ionicons.ellipsis_horizontal_outline),
              )
            ],
          ),
        ),
        //posts
        Container(
          height: 300,
          child: CarouselWithIndicatorDemo(post),
        ),
        //Comments
        Padding(
          padding: const EdgeInsets.only(left: 16.0),
          child: Row(
            children: [
              Text(
                widget.document['likes'].toString(),
                style: TextStyle(fontWeight: FontWeight.bold),
              ),
              Text(
                ' likes',
              ),
            ],
          ),
        ),
        //Captions
        Padding(
          padding: const EdgeInsets.only(left: 16.0, top: 16, bottom: 16),
          child: RichText(
            text: TextSpan(
              style: TextStyle(color: Colors.black),
              children: [
                TextSpan(
                    text: widget.document['name'],
                    style: TextStyle(fontWeight: FontWeight.bold)),
                TextSpan(text: widget.document['description'])
              ],
            ),
          ),
        ),
        Divider(),
      ],
    );
  }
}

// final List<String> imgList = [
//   'assets/images/image1.jpg',
//   'assets/images/image2.jpg',
// ];

// final List<Widget> imageSliders = imgList
//     .map((item) => ClipRRect(
//             child: Stack(
//           children: <Widget>[
//             Image.asset(
//               item,
//               fit: BoxFit.cover,
//               width: 1000.0,
//             ),
//             Positioned(
//               top: 12.0,
//               right: 12.0,
//               child: Container(
//                 decoration: BoxDecoration(
//                     color: Color.fromRGBO(15, 15, 16, 0.7),
//                     borderRadius: BorderRadius.all(Radius.circular(25))),
//                 padding: EdgeInsets.all(8),
//                 child: Text(
//                   '${imgList.indexOf(item) + 1} / ${imgList.length}',
//                   style: TextStyle(
//                     color: Colors.white,
//                     fontSize: 12.0,
//                   ),
//                 ),
//               ),
//             ),
//           ],
//         )))
//     .toList();

// ignore: must_be_immutable
class CarouselWithIndicatorDemo extends StatefulWidget {
  QueryDocumentSnapshot<Object?> userPost;
  CarouselWithIndicatorDemo(this.userPost);
  @override
  State<StatefulWidget> createState() {
    return _CarouselWithIndicatorState();
  }
}

class _CarouselWithIndicatorState extends State<CarouselWithIndicatorDemo> {
  int _current = 0;
  final CarouselController _controller = CarouselController();
  @override
  Widget build(BuildContext context) {
    bool isHearted = widget.userPost["isLiked"];
    bool isBookmarked = widget.userPost["isSaved"];
    List<String> images = [];
    widget.userPost["images"].forEach((e) => images.add(e.toString()));

    final List<Widget> imageSliders = images
        .map((item) => ClipRRect(
                child: Stack(
              children: <Widget>[
                Image.network(
                  item,
                  fit: BoxFit.cover,
                  width: 1000.0,
                ),
                Positioned(
                  top: 12.0,
                  right: 12.0,
                  child: Container(
                    decoration: BoxDecoration(
                        color: Color.fromRGBO(15, 15, 16, 0.7),
                        borderRadius: BorderRadius.all(Radius.circular(25))),
                    padding: EdgeInsets.all(8),
                    child: Text(
                      '${images.indexOf(item) + 1} / ${images.length}',
                      style: TextStyle(
                        color: Colors.white,
                        fontSize: 12.0,
                      ),
                    ),
                  ),
                )
              ],
            )))
        .toList();

    final double height = MediaQuery.of(context).size.height;
    return Scaffold(
      body: Column(children: [
        Expanded(
          child: CarouselSlider(
            items: imageSliders,
            carouselController: _controller,
            options: CarouselOptions(
                height: height * 1.5,
                viewportFraction: 1.0,
                autoPlay: false,
                enlargeCenterPage: false,
                aspectRatio: 16 / 9,
                onPageChanged: (index, reason) {
                  setState(() {
                    _current = index;
                  });
                }),
          ),
        ),
        Padding(
          padding:
              const EdgeInsets.only(top: 16, left: 4, right: 4, bottom: 16),
          child: Row(
            mainAxisAlignment: MainAxisAlignment.spaceBetween,
            children: [
              Row(
                children: [
                  IconButton(
                    splashRadius: 10,
                    iconSize: 28,
                    icon: Padding(
                      padding: EdgeInsets.zero,
                      child: isHearted == true
                          ? Icon(
                              Ionicons.heart_sharp,
                              color: Colors.red.shade600,
                            )
                          : Icon(Ionicons.heart_outline),
                    ),
                    onPressed: () {
                      setState(() {
                        DocumentReference posts = FirebaseFirestore.instance
                            .collection("posts")
                            .doc(widget.userPost.id);
                        posts.update({
                          'isLiked': !isHearted,
                          'likes': isHearted == false
                              ? widget.userPost['likes'] + 1
                              : widget.userPost['likes'] - 1
                        });
                      });
                    },
                  ),
                  IconButton(
                      splashRadius: 10,
                      onPressed: () {},
                      icon: Icon(
                        Ionicons.chatbubble_outline,
                        size: 25,
                      )),
                  IconButton(
                      splashRadius: 10,
                      onPressed: () {},
                      icon: Icon(
                        Ionicons.paper_plane_outline,
                        size: 25,
                      ))
                ],
              ),
              Row(
                mainAxisAlignment: MainAxisAlignment.center,
                children: images.asMap().entries.map((entry) {
                  return GestureDetector(
                    onTap: () => _controller.animateToPage(entry.key),
                    child: Container(
                      width: 8.0,
                      height: 8.0,
                      margin:
                          EdgeInsets.symmetric(vertical: 8.0, horizontal: 4.0),
                      decoration: BoxDecoration(
                          shape: BoxShape.circle,
                          color: Colors.blue
                              .withOpacity(_current == entry.key ? 1 : 0.2)),
                    ),
                  );
                }).toList(),
              ),
              IconButton(
                // splashColor: Colors.transparent,
                splashRadius: 10,
                iconSize: 25,
                icon: Padding(
                  padding: EdgeInsets.zero,
                  child: isBookmarked == true
                      ? Icon(
                          Ionicons.bookmark_sharp,
                        )
                      : Icon(
                          Ionicons.bookmark_outline,
                        ),
                ),
                onPressed: () {
                  setState(() {
                    DocumentReference posts = FirebaseFirestore.instance
                        .collection("posts")
                        .doc(widget.userPost.id);
                    posts.update({'isSaved': !isBookmarked});
                  });
                },
              ),
            ],
          ),
        ),
      ]),
    );
  }
}
