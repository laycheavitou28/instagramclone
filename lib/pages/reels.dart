import 'package:cloud_firestore/cloud_firestore.dart';
import 'package:flutter/material.dart';
import 'package:image_picker/image_picker.dart';
import 'package:ionicons/ionicons.dart';
import 'dart:io';
import 'package:firebase_storage/firebase_storage.dart' as firebase_storage;
import 'package:uuid/uuid.dart';

import '../homepage.dart';

class UserReels extends StatefulWidget {
  @override
  State<UserReels> createState() => _UserReelsState();
}

class _UserReelsState extends State<UserReels> {
  final Stream<QuerySnapshot> users =
      FirebaseFirestore.instance.collection('testing').snapshots();

  var name = 'Vitou';
  var profile =
      'https://firebasestorage.googleapis.com/v0/b/mobileproject-1c6ea.appspot.com/o/fanciest.jpg?alt=media&token=c383a662-899e-41c2-adb3-eac27091eb79';

  @override
  Widget build(BuildContext context) {
    return Scaffold(
        body: Padding(
      padding: const EdgeInsets.all(16.0),
      child: SingleChildScrollView(
        child: Column(children: [
          Profile(profile: profile, name: name),
          CreatePostsForm(),
        ]),
      ),
    ));
  }
}

class Profile extends StatelessWidget {
  const Profile({
    Key? key,
    required this.profile,
    required this.name,
  }) : super(key: key);

  final String profile;
  final String name;

  @override
  Widget build(BuildContext context) {
    return Row(
      children: [
        // profile photo
        Container(
          width: 45,
          height: 45,
          decoration: BoxDecoration(
              shape: BoxShape.circle,
              image: DecorationImage(
                  image: NetworkImage(profile), fit: BoxFit.fill)),
        ),
        SizedBox(
          width: 10,
        ),
        Row(
          children: [
            Text(
              name,
              // ignore: prefer_const_constructors
              style: TextStyle(fontWeight: FontWeight.bold, fontSize: 15),
            ),
            Padding(
              padding: const EdgeInsets.only(left: 4.0),
              child: Image.asset(
                'assets/images/check.png',
              ),
            )
          ],
        ),
      ],
    );
  }
}

class CreatePostsForm extends StatefulWidget {
  const CreatePostsForm({
    Key? key,
  }) : super(key: key);

  @override
  State<CreatePostsForm> createState() => _CreatePostsFormState();
}

class _CreatePostsFormState extends State<CreatePostsForm> {
  final _formKey = GlobalKey<FormState>();
  List<dynamic> imageFile = [];

  var name = 'Vitou ';
  var description = '';
  bool isLiked = false;
  bool isSaved = false;
  var likes = 0;
  var images = [
    'https://firebasestorage.googleapis.com/v0/b/mobileproject-1c6ea.appspot.com/o/Blue-sky.jpg?alt=media&token=a1d7ff46-35ce-47c8-886e-b6f590af22d9',
    'https://firebasestorage.googleapis.com/v0/b/mobileproject-1c6ea.appspot.com/o/fanciest.jpg?alt=media&token=c383a662-899e-41c2-adb3-eac27091eb79'
  ];
  var profile =
      'https://firebasestorage.googleapis.com/v0/b/mobileproject-1c6ea.appspot.com/o/fanciest.jpg?alt=media&token=c383a662-899e-41c2-adb3-eac27091eb79';

  @override
  Widget build(BuildContext context) {
    CollectionReference posts = FirebaseFirestore.instance.collection('posts');

    clearimage(index) {
      setState(() {
        imageFile.removeAt(index);
        // _controller.setImage(imageFile);
      });
    }

    return Form(
        key: _formKey,
        child: Column(
          crossAxisAlignment: CrossAxisAlignment.start,
          children: [
            imageFile.isEmpty
                ? Container()
                : Container(
                    height: 100,
                    margin: EdgeInsets.symmetric(
                      horizontal: 10.0,
                      vertical: 20.0,
                    ),
                    child: ListView.builder(
                      scrollDirection: Axis.horizontal,
                      itemBuilder: (ctx, i) => Container(
                        height: 100,
                        child: Row(
                          children: [
                            Padding(
                                padding: EdgeInsets.fromLTRB(5, 0, 0, 0),
                                child: Container(
                                  height: 100,
                                  child: Image.file(
                                    imageFile[i],
                                    fit: BoxFit.fitHeight,
                                  ),
                                )),
                            Padding(
                              padding: EdgeInsets.fromLTRB(0, 0, 0, 90),
                              child: GestureDetector(
                                onTap: () {
                                  clearimage(i);
                                },
                                child: Icon(
                                  Icons.close,
                                  size: 20,
                                  color: Colors.black,
                                ),
                              ),
                            ),
                          ],
                        ),
                      ),
                      itemCount: imageFile.length,
                    ),
                  ),
            Row(
              children: [
                Expanded(
                  child: Padding(
                    padding: const EdgeInsets.all(8.0),
                    child: TextFormField(
                      decoration: InputDecoration(
                        hintText: 'Write a caption...',
                      ),
                      onChanged: (value) {
                        description = value;
                      },
                      validator: (value) {
                        if (value == null || value.isEmpty) {
                          return 'Please enter some text';
                        }
                        return null;
                      },
                    ),
                  ),
                ),
                IconButton(
                    onPressed: () async {
                      XFile? pickedFile = await ImagePicker().pickImage(
                        source: ImageSource.gallery,
                        imageQuality: 25,
                        maxWidth: 1800,
                        maxHeight: 1800,
                      );
                      if (pickedFile != null) {
                        setState(() {
                          imageFile.add(File(pickedFile.path));
                        });
                      }
                    },
                    icon: Icon(Ionicons.images_outline)),
              ],
            ),
            Center(
              child: Padding(
                padding: const EdgeInsets.all(16.0),
                child: Container(
                  height: 40,
                  width: 200,
                  decoration: BoxDecoration(
                      shape: BoxShape.rectangle,
                      border:
                          Border.all(width: 1.0, color: Colors.grey.shade400),
                      borderRadius: BorderRadius.circular(5)),
                  child: TextButton(
                    onPressed: () async {
                      if (_formKey.currentState!.validate()) {
                        ScaffoldMessenger.of(context).showSnackBar(
                            SnackBar(content: Text('Uploading new post...')));

                        List<String> url;

                        Future<List<String>> uploadFiles(
                            List<dynamic> _images) async {
                          List<String> imagesUrls = [];
                          var uuid = Uuid();
                          await Future.wait(_images.map((input) async {
                            firebase_storage.Reference ref = firebase_storage
                                .FirebaseStorage.instance
                                .ref()
                                .child('uploads')
                                .child(uuid.v4());
                            await ref.putFile(input);

                            var url = await ref.getDownloadURL();
                            imagesUrls.add(url);
                          }));

                          return imagesUrls;
                        }

                        url = await uploadFiles(imageFile);

                        await posts
                            .add({
                              'name': name,
                              'description': description,
                              'isLiked': isLiked,
                              'isSaved': isSaved,
                              'images': url,
                              'profile': profile,
                              'likes': likes
                            })
                            .then((value) => ScaffoldMessenger.of(context)
                                .showSnackBar(SnackBar(
                                    content: Text('Post Added Succesfully'))))
                            .catchError((error) =>
                                // ignore: invalid_return_type_for_catch_error, avoid_print
                                print('Failed to create new posts: $error'));

                        await Navigator.push(
                            context,
                            MaterialPageRoute(
                                builder: (BuildContext context) => HomePage()));
                      }
                    },
                    child: Text(
                      'Post',
                      style: TextStyle(
                          fontWeight: FontWeight.bold,
                          color: Colors.black,
                          fontSize: 16),
                    ),
                  ),
                ),
              ),
            ),
          ],
        ));
  }
}
