import 'package:cloud_firestore/cloud_firestore.dart';
import 'package:flutter/material.dart';

class UserSearch extends StatelessWidget {
  @override
  Widget build(BuildContext context) {
    return Scaffold(
      body: StreamBuilder(
        stream: FirebaseFirestore.instance.collection('users').snapshots(),
        builder: (BuildContext context, AsyncSnapshot<QuerySnapshot> snapshot) {
          if (!snapshot.hasData) {
            return Center(
              child: CircularProgressIndicator(),
            );
          }
          // var documents = snapshot.data!.docs.toList();
          return ListView(
            children: snapshot.data!.docs.map((document) {
              return Center(
                child: Text(document['description']),
              );
            }).toList(),
          );
        },
      ),
    );
  }
}
