// import 'dart:typed_data';

// import 'package:flutter/material.dart';
// import 'package:image_picker/image_picker.dart';
// import 'package:instragram_clone/util/utils.dart';

// class UserShop extends StatefulWidget {
//   @override
//   State<UserShop> createState() => _UserShopState();
// }

// class _UserShopState extends State<UserShop> {
//   Uint8List? _file;

//   // void postImage(String uid, String) {}

//   final TextEditingController _descriptionController = TextEditingController();
//   var profile =
//       'https://firebasestorage.googleapis.com/v0/b/mobileproject-1c6ea.appspot.com/o/fanciest.jpg?alt=media&token=c383a662-899e-41c2-adb3-eac27091eb79';

//   _selectImage(BuildContext context) async {
//     return showDialog(
//         context: context,
//         builder: (context) {
//           return SimpleDialog(
//             title: Text('Create a post'),
//             children: [
//               SimpleDialogOption(
//                 padding: EdgeInsets.all(20),
//                 child: Text('Take a photo'),
//                 onPressed: () async {
//                   Navigator.of(context).pop();
//                   Uint8List file = await pickImage(
//                     ImageSource.camera,
//                   );
//                   setState(() {
//                     _file = file;
//                   });
//                 },
//               ),
//               SimpleDialogOption(
//                 padding: EdgeInsets.all(20),
//                 child: Text('Choose from gallery'),
//                 onPressed: () async {
//                   Navigator.of(context).pop();
//                   Uint8List file = await pickImage(
//                     ImageSource.gallery,
//                   );
//                   setState(() {
//                     _file = file;
//                   });
//                 },
//               ),
//               SimpleDialogOption(
//                 padding: EdgeInsets.all(20),
//                 child: Text('Cancel'),
//                 onPressed: () async {
//                   Navigator.of(context).pop();
//                 },
//               )
//             ],
//           );
//         });
//   }

//   @override
//   void dispose() {
//     super.dispose();
//     _descriptionController.dispose();
//   }

//   @override
//   Widget build(BuildContext context) {
//     return _file == null
//         ? Center(
//             child: IconButton(
//               icon: Icon(Icons.upload),
//               onPressed: () => _selectImage(context),
//             ),
//           )
//         : Scaffold(
//             appBar: AppBar(
//               leading: IconButton(
//                 icon: Icon(Icons.arrow_back),
//                 onPressed: () {},
//               ),
//               foregroundColor: Colors.black,
//               backgroundColor: Colors.white,
//               shadowColor: Colors.transparent,
//               title: Text('Post to'),
//               actions: [
//                 TextButton(
//                     onPressed: () {},
//                     child: Text(
//                       'Post',
//                       style: TextStyle(
//                           color: Colors.blueAccent,
//                           fontWeight: FontWeight.bold,
//                           fontSize: 16),
//                     ))
//               ],
//             ),
//             body: Column(
//               children: [
//                 Row(
//                   mainAxisAlignment: MainAxisAlignment.spaceAround,
//                   crossAxisAlignment: CrossAxisAlignment.start,
//                   children: [
//                     CircleAvatar(
//                       backgroundImage: NetworkImage(profile),
//                     ),
//                     SizedBox(
//                       width: MediaQuery.of(context).size.width * 0.45,
//                       child: TextField(
//                         controller: _descriptionController,
//                         decoration: InputDecoration(
//                           hintText: 'Write a caption...',
//                           border: InputBorder.none,
//                         ),
//                         maxLines: 8,
//                       ),
//                     ),
//                     SizedBox(
//                       height: 45,
//                       width: 45,
//                       child: AspectRatio(
//                         aspectRatio: 487 / 451,
//                         child: Container(
//                             decoration: BoxDecoration(
//                                 image: DecorationImage(
//                                     image: MemoryImage(_file!),
//                                     fit: BoxFit.fill,
//                                     alignment: FractionalOffset.topCenter))),
//                       ),
//                     ),
//                     Divider(),
//                   ],
//                 )
//               ],
//             ),
//           );
//   }
// }
