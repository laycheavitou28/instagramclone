import 'package:cloud_firestore/cloud_firestore.dart';
import 'package:flutter/material.dart';
import 'package:instragram_clone/util/bubble_stories.dart';
import 'package:instragram_clone/util/user_posts.dart';
import 'package:ionicons/ionicons.dart';

class UserHome extends StatelessWidget {
  final List people = [
    'Person1',
    'Person2',
    'Person3',
    'Person4',
    'Person5',
    'Person6'
  ];

  final List profile = [
    'assets/images/image1.jpg',
    'assets/images/image2.jpg',
    'assets/images/image1.jpg',
    'assets/images/image2.jpg',
    'assets/images/image1.jpg',
    'assets/images/image2.jpg',
  ];

  @override
  Widget build(BuildContext context) {
    return Scaffold(
      body: SingleChildScrollView(
        child: Column(
          children: [
            TopBar(),
            Divider(),
            Stories(people: people, profile: profile),
            Divider(),
            StreamBuilder(
              stream: FirebaseFirestore.instance
                  .collection('posts')
                  .orderBy('likes', descending: true)
                  .snapshots(),
              builder: (BuildContext context,
                  AsyncSnapshot<QuerySnapshot> snapshot) {
                if (!snapshot.hasData) {
                  return Center(
                    child: CircularProgressIndicator(),
                  );
                }
                return ListView(
                  shrinkWrap: true,
                  physics: NeverScrollableScrollPhysics(),
                  children: snapshot.data!.docs.map((document) {
                    return Column(
                      children: [
                        Posts(document: snapshot.data!.docs),
                      ],
                    );
                  }).toList(),
                );
              },
            ),
          ],
        ),
      ),
    );
  }
}

// ignore: must_be_immutable
class Posts extends StatelessWidget {
  Posts({Key? key, required this.document}) : super(key: key);

  List<QueryDocumentSnapshot<Object?>> document;

  @override
  Widget build(BuildContext context) {
    return Container(
      height: 450,
      child: ListView.builder(
          shrinkWrap: true,
          itemCount: document.length,
          itemBuilder: (context, index) {
            return UserPosts(document: document[index]);
          }),
    );
  }
}

class Stories extends StatelessWidget {
  const Stories({Key? key, required this.people, required this.profile})
      : super(key: key);

  final List people;
  final List profile;

  @override
  Widget build(BuildContext context) {
    return Container(
      height: 107,
      child: ListView.builder(
          scrollDirection: Axis.horizontal,
          itemCount: people.length,
          itemBuilder: (context, index) {
            return BubbleStories(text: people[index], img: profile[index]);
          }),
    );
  }
}

class TopBar extends StatelessWidget {
  const TopBar({
    Key? key,
  }) : super(key: key);

  @override
  Widget build(BuildContext context) {
    return SizedBox(
      height: 58,
      child: Padding(
        padding: const EdgeInsets.only(top: 12, left: 8, right: 8),
        child: Center(
          child: Row(
            crossAxisAlignment: CrossAxisAlignment.center,
            mainAxisAlignment: MainAxisAlignment.center,
            children: [
              IconButton(
                splashRadius: 10,
                onPressed: () {},
                icon: Icon(
                  Ionicons.camera_outline,
                  size: 30,
                ),
              ),
              Spacer(),
              Center(
                child: Image.asset(
                  'assets/images/logo.png',
                  height: 30,
                ),
              ),
              Spacer(),
              Row(
                children: [
                  // Padding(
                  //   padding: const EdgeInsets.only(right: 16),
                  //   child: IconButton(
                  //     splashRadius: 10,
                  //     onPressed: () {},
                  //     icon: Icon(
                  //       Ionicons.mail_unread_outline,
                  //       size: 30,
                  //     ),
                  //   ),
                  // ),
                  IconButton(
                    splashRadius: 10,
                    onPressed: () {},
                    icon: Icon(
                      Ionicons.paper_plane_outline,
                      size: 30,
                    ),
                  ),
                ],
              ),
            ],
          ),
        ),
      ),
    );
  }
}
