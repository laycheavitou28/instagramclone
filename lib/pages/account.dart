import 'package:cloud_firestore/cloud_firestore.dart';
import 'package:flutter/material.dart';
import 'package:instragram_clone/accountsetting.dart';
import 'package:instragram_clone/loginpage.dart';
import 'package:instragram_clone/util/account_stories.dart';
import 'package:ionicons/ionicons.dart';

class UserAccount extends StatelessWidget {
  @override
  Widget build(BuildContext context) {
    return Scaffold(
      body: SingleChildScrollView(
        child: Column(
          crossAxisAlignment: CrossAxisAlignment.start,
          children: [
            StreamBuilder(
              stream:
                  FirebaseFirestore.instance.collection('users').snapshots(),
              builder: (BuildContext context,
                  AsyncSnapshot<QuerySnapshot> snapshot) {
                if (!snapshot.hasData) {
                  return Center(
                    child: CircularProgressIndicator(),
                  );
                }
                // var documents = snapshot.data!.docs.toList();
                return ListView(
                  shrinkWrap: true,
                  children: snapshot.data!.docs.map((document) {
                    return Column(
                      crossAxisAlignment: CrossAxisAlignment.start,
                      children: [
                        AccountTopBar(document: document),
                        ProfileAndStats(document: document),
                        ProfileDescription(document: document),
                        EditButton(),
                        // RecentStories(images: images, name: name),
                        Divider(),
                        ViewList(document: document),
                        // GridList(items: items)
                      ],
                    );
                  }).toList(),
                );
              },
            ),
          ],
        ),
      ),
    );
  }
}

class GridList extends StatelessWidget {
  const GridList({
    Key? key,
    required this.document,
  }) : super(key: key);

  final QueryDocumentSnapshot<Object?> document;

  @override
  Widget build(BuildContext context) {
    return Padding(
      padding: const EdgeInsets.only(top: 1.0),
      child: GridView.builder(
          gridDelegate: SliverGridDelegateWithFixedCrossAxisCount(
              crossAxisCount: 3, mainAxisSpacing: 3, crossAxisSpacing: 3),
          itemCount: document['allPhotos'].length,
          itemBuilder: (context, index) {
            return Container(
              decoration: BoxDecoration(
                image: DecorationImage(
                  fit: BoxFit.cover,
                  image: NetworkImage(document['allPhotos'][index]),
                ),
              ),
            );
          }),
    );
  }
}

class ViewList extends StatelessWidget {
  const ViewList({Key? key, required this.document}) : super(key: key);

  final QueryDocumentSnapshot<Object?> document;

  @override
  Widget build(BuildContext context) {
    return DefaultTabController(
        length: 1,
        child: Column(
          children: [
            TabBar(
              indicatorColor: Colors.black,
              indicatorWeight: 2,
              tabs: [
                Tab(
                  icon: Icon(
                    Ionicons.apps_sharp,
                    color: Colors.black,
                    size: 30,
                  ),
                ),
                // Tab(
                //   icon: Icon(
                //     Ionicons.bookmark_sharp,
                //     color: Colors.black,
                //     size: 30,
                //   ),
                // )
              ],
            ),
            Container(
              height: 275,
              child: TabBarView(
                physics: ClampingScrollPhysics(),
                children: [
                  GridList(document: document),
                  // GridList(document: document)
                ],
              ),
            )
          ],
        ));
  }
}

class RecentStories extends StatelessWidget {
  const RecentStories({Key? key, required this.name, required this.images})
      : super(key: key);

  final List name;
  final List images;

  @override
  Widget build(BuildContext context) {
    return SingleChildScrollView(
      scrollDirection: Axis.horizontal,
      child: Row(
        crossAxisAlignment: CrossAxisAlignment.center,
        mainAxisAlignment: MainAxisAlignment.center,
        children: [
          // AddButton(),
          Column(
            children: [
              Padding(
                padding: const EdgeInsets.only(left: 8.0, right: 8),
                child: Container(
                  height: 75,
                  width: 75,
                  decoration: BoxDecoration(
                    color: Colors.white,
                    border: Border.all(color: Colors.grey.shade500),
                    borderRadius: BorderRadius.circular(100.0),
                  ),
                  child: Icon(Ionicons.add_outline),
                ),
              ),
              Padding(
                padding: const EdgeInsets.all(8.0),
                child: Text('New'),
              ),
            ],
          ),
          Container(
            height: 126.0,
            width: 90.0 * name.length,
            child: ListView.builder(
              scrollDirection: Axis.horizontal,
              physics: NeverScrollableScrollPhysics(),
              itemCount: name.length,
              itemBuilder: (context, index) {
                return AccountStories(name: name[index], img: images[index]);
              },
            ),
          ),
        ],
      ),
    );
  }
}

class AddButton extends StatelessWidget {
  const AddButton({
    Key? key,
  }) : super(key: key);

  @override
  Widget build(BuildContext context) {
    return ElevatedButton(
      onPressed: () {},
      child: Icon(Ionicons.add_outline, color: Colors.black),
      style: ElevatedButton.styleFrom(
        shape: CircleBorder(),
        padding: EdgeInsets.all(20),
        primary: Colors.white, // <-- Button color
        onPrimary: Colors.grey, // <-- Splash color
      ),
    );
  }
}

class EditButton extends StatelessWidget {
  const EditButton({
    Key? key,
  }) : super(key: key);

  @override
  Widget build(BuildContext context) {
    return Padding(
      padding: const EdgeInsets.only(top: 40.0, left: 16, right: 16),
      child: Container(
        height: 35,
        decoration: BoxDecoration(
            shape: BoxShape.rectangle,
            border: Border.all(width: 1.0, color: Colors.grey.shade400),
            borderRadius: BorderRadius.circular(5)),
        child: TextButton(
          onPressed: () {
            Navigator.push(context,
                MaterialPageRoute(builder: (context) => AccountSetting()));
          },
          child: const Text(
            'Edit Profile',
            style: TextStyle(
                fontWeight: FontWeight.bold, color: Colors.black, fontSize: 16),
          ),
          style: TextButton.styleFrom(minimumSize: Size.fromHeight(1)),
        ),
      ),
    );
  }
}

class ProfileDescription extends StatelessWidget {
  const ProfileDescription({Key? key, required this.document})
      : super(key: key);

  final QueryDocumentSnapshot<Object?> document;

  @override
  Widget build(BuildContext context) {
    return Padding(
      padding: const EdgeInsets.only(left: 30, top: 8),
      child: Column(
        crossAxisAlignment: CrossAxisAlignment.start,
        children: [
          Text(
            document['name'],
            style: TextStyle(fontWeight: FontWeight.bold, fontSize: 14),
          ),
          Padding(
            padding: const EdgeInsets.only(top: 2.0),
            child: Text(document['description']),
          )
        ],
      ),
    );
  }
}

// ignore: must_be_immutable
class ProfileAndStats extends StatelessWidget {
  ProfileAndStats({Key? key, required this.document}) : super(key: key);

  final QueryDocumentSnapshot<Object?> document;
  CollectionReference posts = FirebaseFirestore.instance.collection('posts');

  @override
  Widget build(BuildContext context) {
    return Padding(
      padding: const EdgeInsets.only(left: 16.0, right: 16.0, bottom: 8.0),
      child: Row(
        children: [
          Container(
            height: 100,
            width: 100,
            decoration: BoxDecoration(
              shape: BoxShape.circle,
              border: Border.all(color: Colors.grey.shade300, width: 1.5),
            ),
            child: Stack(
              children: [
                Container(
                  decoration: BoxDecoration(
                    shape: BoxShape.circle,
                  ),
                  child: Center(
                    child: ClipRRect(
                      borderRadius: BorderRadius.circular(45),
                      child: Image.network(
                        document['profileImage'],
                        fit: BoxFit.fill,
                        width: 90,
                        height: 90,
                      ),
                    ),
                  ),
                )
              ],
            ),
          ),
          Row(
            children: [
              Padding(
                padding: const EdgeInsets.only(left: 20.0),
                child: Column(
                  children: [
                    Text(
                      document['postnumbers'].toString(),
                      style:
                          TextStyle(fontWeight: FontWeight.bold, fontSize: 18),
                    ),
                    Text('Posts')
                  ],
                ),
              ),
              Padding(
                padding: const EdgeInsets.only(left: 20.0),
                child: Column(
                  children: [
                    Text(
                      document['followers'].toString(),
                      style:
                          TextStyle(fontWeight: FontWeight.bold, fontSize: 18),
                    ),
                    Text('Followers')
                  ],
                ),
              ),
              Padding(
                padding: const EdgeInsets.only(left: 20.0),
                child: Column(
                  children: [
                    Text(
                      document['following'].toString(),
                      style:
                          TextStyle(fontWeight: FontWeight.bold, fontSize: 18),
                    ),
                    Text('Following')
                  ],
                ),
              ),
            ],
          )
        ],
      ),
    );
  }
}

class AccountTopBar extends StatelessWidget {
  const AccountTopBar({Key? key, required this.document}) : super(key: key);
  final QueryDocumentSnapshot<Object?> document;
  @override
  Widget build(BuildContext context) {
    return Padding(
      padding: const EdgeInsets.all(16.0),
      child: Row(
        crossAxisAlignment: CrossAxisAlignment.center,
        children: [
          Spacer(),
          Icon(
            Ionicons.lock_closed,
            size: 15,
          ),
          Padding(
            padding: const EdgeInsets.symmetric(horizontal: 2),
            child: Text(
              document["name"],
              style: TextStyle(fontWeight: FontWeight.bold, fontSize: 18),
            ),
          ),
          Icon(
            Ionicons.chevron_down,
            size: 18,
          ),
          Spacer(),
          IconButton(
              onPressed: () {
                Navigator.push(context,
                    MaterialPageRoute(builder: (context) => LoginPage()));
              },
              icon: Icon(
                Ionicons.log_out_outline,
                size: 30,
              )),
          // IconButton(
          //     icon: Icon(
          //       Ionicons.menu,
          //       size: 30,
          //     ),
          //     onPressed: () {
          //       Navigator.push(context,
          //           MaterialPageRoute(builder: (context) => AccountSetting()));
          //     })
        ],
      ),
    );
  }
}
